package org.steinko.apigateway;

public class APIGatewayRequest {
	
private String httpMethod;

APIGatewayRequest(){
	
}

APIGatewayRequest(String httpMethod){
	this.httpMethod = httpMethod;
}

public String getHttpMethod() {
	return httpMethod;
}

public void setHttpMethod(String httpMethod) {
	this.httpMethod = httpMethod;
}


}
