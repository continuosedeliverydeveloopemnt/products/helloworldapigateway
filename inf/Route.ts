import * as aws from "@pulumi/aws";
import * as pulumi from "@pulumi/pulumi";
import {api}  from "./ApiGateway"
import {apiGateWayAutoriser}  from "./ApiGatewayAutoriser"
import {integration}  from "./Integration"


export const route = new aws.apigatewayv2.Route("route", {
    apiId: api.id,
    routeKey: "GET /",
    authorizationType: "AWS_IAM",
    authoriserId: apiGateWayAutoriser.id
});