import * as aws from "@pulumi/aws";
import {api}  from "./ApiGateway"
import {lamdaFunction}  from "./Function"



export const integration = new aws.apigatewayv2.Integration("integration", {
    apiId: api.id,
    integrationType: "HTTP_PROXY",
    connectionType: "INTERNET",
    //contentHandlingStrategy: "CONVERT_TO_TEXT",
    integrationMethod: "GET",
    integrationUri: lamdaFunction.invokeArn,
    passthroughBehavior: "WHEN_NO_MATCH",
});