import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
 import {api}  from "./ApiGateway"
import {apiGatewayRole}  from "./ApiGatewayRole"

const example = new aws.apigatewayv2.Authorizer("example", {
    apiId: api.id,
    authorizerType: "REQUEST",
    authorizerCredentialsArn:apiGatewayRole.arn,
    authorizerPayloadFormatVersion: "2.0",
});