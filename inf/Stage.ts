import * as aws from "@pulumi/aws";
import {api}  from "./ApiGateway"
import {testLog} from "./TestLog"
import {apiGatewayDeployment} from "./ApiGatewayDeployment"

export const stage = new aws.apigatewayv2.Stage("example", 
{ apiId: api.id,
  autoDeploy: true,
  deploymentId: apiGatewayDeployment.id,
  accessLogSettings:{
		destinationArn : testLog.arn,
		format: '{ "requestId":"$context.requestId", "ip": "$context.identity.sourceIp", "requestTime":"$context.requestTime", "httpMethod":"$context.httpMethod","routeKey":"$context.routeKey", "status":"$context.status","protocol":"$context.protocol", "responseLength":"$context.responseLength" }'
	    },
  defaultRouteSettings: { 
	     dataTraceEnabled:true,
	     detailedMetricsEnabled:true,
	     loggingLevel: "ERROR"  
        },
   name:"$default",
   
	 });