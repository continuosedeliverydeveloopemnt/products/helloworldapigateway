import { LambdaClient, InvokeCommand,GetFunctionConfigurationCommand } from "@aws-sdk/client-lambda";

let client
let input
let command

beforeEach( async() => {
   const config = {}
   client = new LambdaClient(config);
   input = {
       FunctionName: "helloworld", 
       InvocationType:  "RequestResponse" 
   }
   command = new InvokeCommand(input);
})

it('statuscode should )be 200"', async ()=> {
 const response = await client.send(command);
   expect(response.StatusCode).toBe(200)
})

it('should deliver JSON with "body": "hello world)"', async ()=> {
   const {Payload} = await client.send(command);
   const result = Buffer.from(Payload).toString();
   const obj = JSON.parse(result)
   expect(obj.body).toBe("Hello World")
})

it('should exist 2 layers', async ()=> {
	
	const command = new GetFunctionConfigurationCommand(input);
    const response = await client.send(command)
    expect(response.Layers.length).toBe(2)
})


it('should exist datadog java agent layer', async ()=> {
	
	const command = new GetFunctionConfigurationCommand(input);
    const response = await client.send(command)
    expect(response.Layers[0].Arn).toBe("arn:aws:lambda:eu-north-1:901920570463:layer:aws-otel-java-agent-arm64-ver-1-24-0:1")
   
})

it('should exist datadog extension layer', async ()=> {
	
	const command = new GetFunctionConfigurationCommand(input);
    const response = await client.send(command)
    expect(response.Layers[1].Arn).toBe("arn:aws:lambda:eu-north-1:464622532012:layer:Datadog-Extension-ARM:43")
})