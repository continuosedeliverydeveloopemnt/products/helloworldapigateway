import * as aws from "@pulumi/aws";
import {lamdaFunction}  from "./Function"
import {api}  from "./ApiGateway"

export const invokeFunctionPolicy = new aws.iam.Policy("imvoke-function-policy", {
    name: "invokeFunctionPolicy",
    policy: JSON.stringify(
      {
         "Version": "2012-10-17",
         "Statement": [
           { "Sid:": "Imvoke",
             "Effect": "Allow"
             "Principal":{
			    "Service": "apigateway.amazonaws.com"}
			  },
			  "Action": "lamda:invokeFunction",
			  "Resource": lamdaFunction.arn,
			   "Condition": {
				   "ArnLike": {
					   api.id + "GET /"
				   }
			   }
			}
		  ] 
		}