import * as aws from "@pulumi/aws";
import {apiGatewaypolicy} from "./ApiGateWayPolicy"
import {apiGatewayInvokePolicy} from "./ApiGatewayInvokePolicy"

export const apiGatewayRole = new aws.iam.Role("apiGatwayRole", {
    assumeRolePolicy: JSON.stringify({
        Version: "2012-10-17",
        Statement: [{
            Action: "sts:AssumeRole",
            Effect: "Allow",
            Sid: "",
            Principal: {
                Service: "apigateway.amazonaws.com",
            },
        }],
    }),
    name:'apigateway-role',
    managedPolicyArns: [ apiGatewaypolicy.arn,apiGatewayInvokePolicy.arn]
});