import * as aws from "@pulumi/aws";
import {lamdaFunction}  from "./Function"
import {api}  from "./ApiGateway"

export const invokeApiGatewayPolicy = new aws.iam.Policy("imvoke-api-gateway-policy", {
    name: "invokeApiGatewayPolicy",
    policy: JSON.stringify(
      {
         "Version": "2012-10-17",
         "Statement": [
           { 
             "Effect": "Allow"
             "Principal": "*"
			  "Action": "execute-api:Invoke",
			  "Resource": [
                "execute-api:/*"
              ]
			}
		  ] 
		})
		}