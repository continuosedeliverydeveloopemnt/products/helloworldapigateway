import * as aws from "@pulumi/aws";
import {testLog} from "./TestLog"

export const testLogStream = new aws.cloudwatch.LogStream("testLogStream", {logGroupName: testLog.name});