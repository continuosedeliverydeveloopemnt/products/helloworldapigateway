import * as aws from "@pulumi/aws";

export const apiGatewayInvokePolicy = new aws.iam.Policy("api-gateway-imvoke-policy", {
    name: "apiGatewayInvokePolicy",
    policy: JSON.stringify(
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "lambda:InvokeFunction",
            "Resource": "*"
        }
    ]
}