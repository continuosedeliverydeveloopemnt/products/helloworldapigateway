package org.steinko.apigateway;

public class APIGatewayResponse { 
	public String body;
	public Integer statusCode;
	
	APIGatewayResponse(Integer statusCode, String body){ 
		this.statusCode= statusCode;
		this.body = body;
	}

	public String body() {
		
		return body;
	}
	
}