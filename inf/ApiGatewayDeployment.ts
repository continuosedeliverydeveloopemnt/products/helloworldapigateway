import * as pulumi from "@pulumi/pulumi"
import * as aws from "@pulumi/aws"

import {api}  from "./ApiGateway"

const  apiGatewayDeployment = new aws.apigatewayv2.Deployment("api-gateway-deployment", {
    apiId: api.id
});